var exec = require('cordova/exec');
var Navigation = {};

Navigation.startLocalization = function (apiKey, buildingId, indoorsListener, successCallback, errorCallback) {
    exec(
        function callback(result) {
            if (result.hasOwnProperty('callbackName')) {
                var callbackName = result.callbackName;
                if (indoorsListener.hasOwnProperty(callbackName)) {
                    indoorsListener[callbackName](result.result);
                } else {
                    console.log("Unknown indoors listener callback " + result.callbackName);
                }
                return;
            }

            if (successCallback) {
                successCallback(result);
            }
        },
        function error(err) {
            if (errorCallback) {
                errorCallback(err);
            }
        },
        'CordovaNavigation',
        'cordovaStartLocalization',
        [apiKey, buildingId]
    );
}

Navigation.stopLocalization = function (successCallback, errorCallback) {
    my_exec('cordovaStopLocalization', null, successCallback, errorCallback);
}

Navigation.getVersion = function (successCallback, errorCallback) {
    my_exec('cordovaGetVersion', null, successCallback, errorCallback);
}

Navigation.toGeoLocation = function (position, successCallback, errorCallback) {
    my_exec('cordovaToGeoLocation', [position], successCallback, errorCallback);
}

Navigation.enablePredefinedRouteSnapping = function (distance, successCallback, errorCallback) {
    my_exec('cordovaEnablePredefinedRouteSnapping', [distance], successCallback, errorCallback);
}

Navigation.enableBatteryReport = function (successCallback, errorCallback) {
    my_exec('cordovaEnableBatteryReport', [], successCallback, errorCallback);
}

Navigation.enableUsServer = function (successCallback, errorCallback) {
    my_exec('cordovaEnableUsServer', [], successCallback, errorCallback);
}

function my_exec(functionName, args, successCallback, errorCallback) {
    exec(
        function callback(result) {
            if (successCallback) {
                successCallback(result);
            }
        },
        function error(err) {
            if (errorCallback) {
                errorCallback(err);
            }
        },
        'CordovaNavigation',
        functionName,
        args
    );
}

module.exports = Navigation;
