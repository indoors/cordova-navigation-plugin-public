# indoo.rs Navigation SDK Cordova Plugin

## Sample app

See [cordova-navigation-test-app-public](https://bitbucket.org/indoors/cordova-navigation-test-app-public) for a sample app.

## Documentation

### Start localization


	/**
	 * @param apiKey: API Key of the Application (e.g. "143ab6c4-ab25-11e6-80f5-76304dec7eb7")
	 * @param buildingId: Building ID  (e.g. 342532)
	 * @param indoorsListener: Callback that will receive information like position updates
	                     (more information below)
	 */
	Navigation.startLocalization(apiKey, buildingId, indoorsListener, successCallback, errorCallback)

Configures and starts the localization.

**Important**: the localization does not automatically stop when the App is brought to the
background. This needs to be done manually if desired (e.g. in the onPause event)


#### IndoorsListener

This is a dictionary of callback functions that can be implemented

Sample:

	indoorsListener = {
	    buildingLoaded: function(building) {
	       console.log(building)
	    },
	    positionUpdated: function(position) {
	       console.log(position)
	    }
	}

The following callbacks exist:

* *buildingLoaded*: is called when the building is successfully loaded and localization is started

```
/**
 * @param building: Information about the Building that has been loaded
 */
buildingLoaded(building)
```

Sample:
```{ "id": 342532, "name": "My beautiful building"}```


* *positionUpdated*: is called when a new position is calculated

```
/**
 * @param position: Position with x, y, z coordinates in mm and z as floor level
 */
positionUpdated(position)
```

Sample:

```
{ "x": 12333, "y": 4542, "z": 1 }
```

* *loadingBuilding*: is called when the building is downloaded

```
/**
 * @param progress: Download progress in percent
 */
loadingBuilding(progress)
```

Sample:
```
{ "progress": 34 }
```

* *orientationUpdated*: is called when the device heading changes

```
/**
 * @param orientation: heading in degrees clockwise positive (building north is 0)
 */
orientationUpdated(orientation)
```

Sample:
```
{ "orientation": 273 }
```

### Stop localization

Stops the localization and frees all resources.
Can only be called when the localization is running.

	Navigation.stopLocalization = function(successCallback, errorCallback)

### Get version

Returns the Version of the indoo.rs SDK in successCallback.
Can only be called when the localization is running.

	Navigation.getVersion = function(successCallback, errorCallback)

Sample result:
	```
	{ "version": "4.3.0" }
	```

### Conversion of building coordinates to GPS coordinates

Takes a position in indoo.rs coordinates as received by positionUpdated.
Returns the specified position (in indoo.rs coordinates) as WGS84 coordinates.

	Navigation.toGeoLocation = function(position, successCallback, errorCallback)

Sample result:
	```
	{ "lon": 16.334825, "lat": 48.2032467 }
	```

### Enable predefined route snapping

Can only be called before the localization was started.

	/**
	 * @param distance: maximum distance in meters to route to snap to (default 5000)
	 */
	Navigation.enablePredefinedRouteSnapping = function(distance, successCallback, errorCallback)

### Success and Error callbacks

Those callbacks are always optional.
They indicate whether there was a problem when calling this function (e.g. wrong parameters)

```
/**
 * @param result (optional): If the function returns a value it will be passed here (e.g. version)
 */
function successCallback(result)
```

```
/**
 * @param error (optional): Error description
 */
function errorCallback(error)
```
