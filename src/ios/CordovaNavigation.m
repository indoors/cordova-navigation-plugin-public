#import "CordovaNavigation.h"
@import CoreLocation;

typedef enum : NSUInteger {
    NavigationStatusStarting,
    NavigationStatusRunning,
    NavigationStatusStopped,
} NavigationStatus;

@implementation CordovaNavigation {
    CDVInvokedUrlCommand *_startLocalizationCommand;
    IDSBuilding *_building;
    NavigationStatus _status;
    NSNumber *_predefinedRouteSnappingDistance;
    NSString *serverUrl;
}

- (void)pluginInitialize
{
    serverUrl = @"api";
    _status = NavigationStatusStopped;
}

- (void)onAppTerminate
{
    _startLocalizationCommand = nil;
    _building = nil;
    _status = NavigationStatusStopped;
}

- (void)cordovaGetVersion:(CDVInvokedUrlCommand *)command
{
    NSString *versionInfo = [IndoorsConfiguration sharedInstance].sdkVersion;

    NSDictionary *jsonObj = [NSDictionary dictionaryWithObjectsAndKeys:versionInfo, @"version",nil];

    [self sendSuccessfulMessageBackToJSWithData:jsonObj andCommand:command alive:NO];
}

- (void)cordovaEnableBatteryReport:(CDVInvokedUrlCommand *)command
{
    NSLog(@"Not supported in iOS");
}

- (void)cordovaEnableUsServer:(CDVInvokedUrlCommand *)command
{
    serverUrl = @"api-us";
}

- (void)cordovaStartLocalization:(CDVInvokedUrlCommand *)command
{
    if (_status != NavigationStatusStopped) {
        [self sendErrorMessageBackToJSForCommand:command withErrorDescription:@{ @"error": @"Localization is already in state Starting or Running." }];
        return;
    }

    [IndoorsConfiguration sharedInstance].loadTiles = false;
    [IndoorsConfiguration sharedInstance].serverInstance = serverUrl;

    _status = NavigationStatusStarting;

    _startLocalizationCommand = command;

    NSString *apiKey = command.arguments[0];
    NSInteger buidlingId = [command.arguments[1] integerValue];

    __unused Indoors *indoors = [[Indoors alloc] initWithLicenseKey:apiKey andServiceDelegate:nil];

    if (_predefinedRouteSnappingDistance > 0) {
      [[Indoors instance] enablePredefinedRouteSnapping];
      [[Indoors instance] setPredefinedRouteSnappingMaxDistance:_predefinedRouteSnappingDistance];
    }

    [[Indoors instance] registerLocationListener:self];

//    [[Indoors instance] enableEvaluationMode:YES];
    IDSBuilding* building = [[IDSBuilding alloc] init];
    building.buildingID = buidlingId;

    [[Indoors instance] getBuilding:building forRequestDelegate:self];

    [self sendSuccessfulMessageBackToJSWithData:[NSDictionary dictionary] andCommand:command alive:YES];
}

- (void)cordovaStopLocalization:(CDVInvokedUrlCommand *)command
{
    if (_status == NavigationStatusStopped) {
        [self sendErrorMessageBackToJSForCommand:command withErrorDescription:@{ @"error": @"Localization is already in state Stopped." }];
        return;
    }

    if (_status == NavigationStatusStarting) {
        [[Indoors instance] cancelGetBuilding];
    } else {
        [[Indoors instance] stopLocalization];
    }

    _startLocalizationCommand = nil;
    _building = nil;

    _status = NavigationStatusStopped;

    [self sendSuccessfulMessageBackToJSWithData:[NSDictionary dictionary] andCommand:command alive:NO];
}

- (void)cordovaToGeoLocation:(CDVInvokedUrlCommand *)command
{
    NSDictionary *json = command.arguments[0];
    IDSCoordinate *coordinate = [[IDSCoordinate alloc] initWithX:[json[@"x"] integerValue] andY:[json[@"y"] integerValue] andFloorLevel:[json[@"z"] integerValue]];
    CLLocation *location = [IndoorsCoordinateUtil geoLocationForCoordinate:coordinate inBuilding:_building];

    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:@{ @"lon": [NSNumber numberWithDouble:location.coordinate.longitude], @"lat": [NSNumber numberWithDouble:location.coordinate.latitude] }];
    if (location.horizontalAccuracy >= 0) {
        result[@"accuracy"] = [NSNumber numberWithDouble:location.horizontalAccuracy];
    }
    [self sendSuccessfulMessageBackToJSWithData:result andCommand:command alive:NO];
}

- (void)cordovaEnablePredefinedRouteSnapping:(CDVInvokedUrlCommand *)command
{
    _predefinedRouteSnappingDistance = command.arguments[0];

    [self sendSuccessfulMessageBackToJSWithData:[NSDictionary dictionary] andCommand:command alive:NO];
}

#pragma mark - Indoors delegates

- (void)loadingBuilding:(NSNumber *)progress
{
    NSLog(@"Loading building progress: %@", progress);

    NSMutableDictionary *jsonObj = [NSMutableDictionary dictionary];
    jsonObj[@"callbackName"] = @"loadingBuilding";
    jsonObj[@"result"] = @{ @"progress": progress };

    if (_startLocalizationCommand) {
        [self sendSuccessfulMessageBackToJSWithData:jsonObj andCommand:_startLocalizationCommand alive:YES];
    }
}

- (void)buildingLoaded:(IDSBuilding *)building
{
    NSLog(@"Building loaded with id: %ld", building.buildingID);

    _building = building;
    _status = NavigationStatusRunning;

    [[Indoors instance] startLocalization];

    NSMutableDictionary *jsonObj = [NSMutableDictionary dictionary];
    jsonObj[@"callbackName"] = @"buildingLoaded";
    jsonObj[@"result"] = @{ @"id": [NSNumber numberWithUnsignedInteger:building.buildingID],
                            @"name": building.name ? : @"",
                            @"description": building.buildingDescription ? : @"" };
    if (_startLocalizationCommand) {
        [self sendSuccessfulMessageBackToJSWithData:jsonObj andCommand:_startLocalizationCommand alive:YES];
    }
}

- (void)loadingBuildingFailed
{
    NSLog(@"Failed loading building.");

    _status = NavigationStatusStopped;
}

- (void)orientationUpdated:(NSNumber *)orientation
{
    NSLog(@"Orientation update: %@", orientation);

    NSMutableDictionary *jsonObj = [NSMutableDictionary dictionary];
    jsonObj[@"callbackName"] = @"orientationUpdated";
    jsonObj[@"result"] = @{ @"orientation": [NSNumber numberWithInteger:[orientation intValue]] };

    if (_startLocalizationCommand) {
        [self sendSuccessfulMessageBackToJSWithData:jsonObj andCommand:_startLocalizationCommand alive:YES];
    }
}

- (void)positionUpdated:(IDSCoordinate *)userPosition
{
    NSLog(@"Position update: %@", userPosition);

    NSMutableDictionary *jsonObj = [NSMutableDictionary dictionary];
    jsonObj[@"callbackName"] = @"positionUpdated";
    jsonObj[@"result"] = @{ @"x": [NSNumber numberWithInteger:userPosition.x],
                            @"y": [NSNumber numberWithInteger:userPosition.y],
                            @"z": [NSNumber numberWithInteger:userPosition.z],
                            @"accuracy": [NSNumber numberWithInteger:userPosition.accuracy] };

    if (_startLocalizationCommand) {
        [self sendSuccessfulMessageBackToJSWithData:jsonObj andCommand:_startLocalizationCommand alive:YES];
    }
}

- (void)changedFloor:(NSInteger)floorLevel withName:(NSString*)name
{
    NSLog(@"Changed floorLevel: %ld, name: %@", floorLevel, name);

    NSMutableDictionary *jsonObj = [NSMutableDictionary dictionary];
    jsonObj[@"callbackName"] = @"changedFloor";
    jsonObj[@"result"] = @{ @"floorLevel": [NSNumber numberWithInteger:floorLevel],
                            @"name": name ? : @"" };

    if (_startLocalizationCommand) {
        [self sendSuccessfulMessageBackToJSWithData:jsonObj andCommand:_startLocalizationCommand alive:YES];
    }
}

- (void)zonesEntered:(NSArray *)zones
{
    NSLog(@"Entered zones: %@", zones);

    NSMutableArray *zonesJSON = [NSMutableArray array];
    for (IDSZone *zone in zones) {
        NSMutableArray *zonePointsJSON = [NSMutableArray array];
        for (IDSCoordinate *coordinate in zone.points) {
            [zonePointsJSON addObject: @{ @"x": [NSNumber numberWithInteger:coordinate.x], @"y": [NSNumber numberWithInteger:coordinate.y] }];
        }
        NSInteger floorLevel = [[_building getFloorById:[NSNumber numberWithInteger:zone.floor_id]] level];
        [zonesJSON addObject:@{ @"name" : zone.name ? : @"",
                                @"description" : zone.zoneDescription ? : @"",
                                @"floorLevel" : [NSNumber numberWithInteger:floorLevel],
                                @"zonePoints" : zonePointsJSON }];
    }

//    NSLog(@"Entered zones json: %@", zonesJSON);

    NSMutableDictionary *jsonObj = [NSMutableDictionary dictionary];
    jsonObj[@"callbackName"] = @"enteredZones";
    jsonObj[@"result"] = @{ @"zones": zonesJSON};

    if (_startLocalizationCommand) {
        [self sendSuccessfulMessageBackToJSWithData:jsonObj andCommand:_startLocalizationCommand alive:YES];
    }
}

#pragma mark - Helpers

- (void)sendSuccessfulMessageBackToJSWithData:(NSDictionary *)jsonObj andCommand:(CDVInvokedUrlCommand*)command alive:(BOOL)isAlive
{
    // Create an instance of CDVPluginResult, with an OK status code.
    // Set the return message as the Dictionary object (jsonObj) to be serialized as JSON in the browser
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:jsonObj];
    [pluginResult setKeepCallbackAsBool:isAlive];

    // Execute sendPluginResult on this plugin's commandDelegate, passing in the instance of CDVPluginResult
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)sendErrorMessageBackToJSForCommand:(CDVInvokedUrlCommand*)command withErrorDescription:(NSDictionary *)jsonObj
{
    // Create an instance of CDVPluginResult, with an ERROR status code.
    // Set the return message as the Dictionary object (jsonObj) to be serialized as JSON in the browser
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:jsonObj];

    // Execute sendPluginResult on this plugin's commandDelegate, passing in the instance of CDVPluginResult
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
