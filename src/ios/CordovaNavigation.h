#import <Cordova/CDV.h>
#import <IndoorsSDK/IndoorsSDK.h>

@interface CordovaNavigation : CDVPlugin <IndoorsLocationListener, LoadingBuildingDelegate>

- (void)cordovaGetVersion:(CDVInvokedUrlCommand *)command;

- (void)cordovaStartLocalization:(CDVInvokedUrlCommand *)command;
- (void)cordovaStopLocalization:(CDVInvokedUrlCommand *)command;

- (void)cordovaToGeoLocation:(CDVInvokedUrlCommand *)command;

- (void)cordovaEnablePredefinedRouteSnapping:(CDVInvokedUrlCommand *)command;

- (void)cordovaEnableBatteryReport:(CDVInvokedUrlCommand *)command;

- (void)cordovaEnableUsServer:(CDVInvokedUrlCommand *)command;

/*
 - (void)cordovaGetVersionInfo:(CDVInvokedUrlCommand *)command;
 - (void)cordovaIsInteractorRunning:(CDVInvokedUrlCommand *)command;

 - (void)cordovaRegisterEventListener:(CDVInvokedUrlCommand *)command;
 - (void)cordovaRemoveEventListener:(CDVInvokedUrlCommand *)command;

 - (void)cordovaRegisterBeaconListener:(CDVInvokedUrlCommand *)command;
 - (void)cordovaRemoveBeaconListener:(CDVInvokedUrlCommand *)command;

 - (void)cordovaRegisterNotificationListener:(CDVInvokedUrlCommand *)command;
 - (void)cordovaRemoveNotificationListener:(CDVInvokedUrlCommand *)command;

 - (void)cordovaStartInteractor:(CDVInvokedUrlCommand *)command;
 - (void)cordovaStopInteractor:(CDVInvokedUrlCommand *)command;

 - (void)cordovaSynchroniseData:(CDVInvokedUrlCommand *)command;
 - (void)cordovaGetAllBeacons:(CDVInvokedUrlCommand *)command;

 - (void)cordovaConfigureInteractor:(CDVInvokedUrlCommand *)command;
 - (void)cordovaConfigureNotification:(CDVInvokedUrlCommand *)command;

 - (void)cordovaSetNotificationFilterIOS:(CDVInvokedUrlCommand *)command;
 - (void)cordovaSetNotificationModifierIOS:(CDVInvokedUrlCommand *)command;
 - (void)cordovaSetNotificationFilterAndroid:(CDVInvokedUrlCommand *)command;
 - (void)cordovaSetNotificationModifierAndroid:(CDVInvokedUrlCommand *)command;
 */
@end
