package rs.indoo.cordova;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.widget.Toast;

import com.customlbs.library.IndoorsFactory;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;

import static android.content.Context.LOCATION_SERVICE;
import com.customlbs.locator.LocatorParams;

/**
 * Created by michael on 10/31/16.
 */

class LocalizationStarter {
	public static final int REQUEST_CODE_LOCATION = 32456;
	static final int REQUEST_CODE_PERMISSIONS = 45521;
	private static final String COARSE = Manifest.permission.ACCESS_COARSE_LOCATION;

	private final CordovaNavigation plugin;
	private CordovaInterface cordova;
	private Activity cordovaActivity;
	private String apiKey;
	private String serverUrl;

	LocalizationStarter(CordovaNavigation plugin) {
		this.plugin = plugin;
		this.cordova = plugin.cordova;
		this.cordovaActivity = cordova.getActivity();
	}

	public void start(String apiKey, String serverUrl) {
		this.apiKey = apiKey;
		this.serverUrl = serverUrl;

		if (cordova.hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
			checkLocationIsEnabled();
		} else {
			requestPermissionsFromUser();
		}
	}

	public void checkLocationIsEnabled() {
		// On android Marshmallow we also need to have active Location Services (GPS or Network based)
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			LocationManager locationManager = (LocationManager) cordovaActivity.getSystemService(LOCATION_SERVICE);
			boolean isNetworkLocationProviderEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			boolean isGPSLocationProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			if (!isGPSLocationProviderEnabled && !isNetworkLocationProviderEnabled) {
				// Only if both providers are disabled we need to ask the user to do something
				Toast.makeText(cordovaActivity, "Location is off, enable it in system settings.", Toast.LENGTH_LONG).show();
				Intent locationInSettingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				cordovaActivity.startActivityForResult(locationInSettingsIntent, REQUEST_CODE_LOCATION);
			} else {
				continueLoading();
			}
		} else {
			continueLoading();
		}
	}

	private void requestPermissionsFromUser() {
		cordova.requestPermission(plugin, REQUEST_CODE_PERMISSIONS, COARSE);
	}

	private void continueLoading() {
		IndoorsFactory.createInstance(cordova.getActivity(), apiKey, plugin);
		LocatorParams.setKServerInstance(serverUrl);
	}
}
