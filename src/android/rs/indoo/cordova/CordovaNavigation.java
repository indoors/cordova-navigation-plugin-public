package rs.indoo.cordova;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.customlbs.library.Indoors;
import com.customlbs.library.IndoorsException;
import com.customlbs.library.IndoorsFactory;
import com.customlbs.library.IndoorsLocationListener;
import com.customlbs.library.LocalizationParameters;
import com.customlbs.library.callbacks.IndoorsServiceCallback;
import com.customlbs.library.callbacks.LoadingBuildingStatus;
import com.customlbs.library.model.Building;
import com.customlbs.library.model.Zone;
import com.customlbs.library.util.IndoorsCoordinateUtil;
import com.customlbs.shared.Coordinate;
import com.customlbs.locator.LocatorParams;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static android.content.Context.LOCATION_SERVICE;


public class CordovaNavigation extends CordovaPlugin implements IndoorsLocationListener, IndoorsServiceCallback {
	private static final String TAG = CordovaNavigation.class.getSimpleName();

	private static final String PERMISSION_DENIED_ERROR = "Permission denied";

	private static final String CORDOVA_START_LOCALIZATION = "cordovaStartLocalization";
	private static final String CORDOVA_STOP_LOCALIZATION = "cordovaStopLocalization";
	private static final String CORDOVA_GET_VERSION = "cordovaGetVersion";
	private static final String CORDOVA_TO_GEO_LOCATION = "cordovaToGeoLocation";
	private static final String CORDOVA_ENABLE_PREDEFINED_ROUTE_SNAPPING = "cordovaEnablePredefinedRouteSnapping";
	private static final String CORDOVA_ENABLE_BATTERY_REPORT = "cordovaEnableBatteryReport";
	private static final String CORDOVA_ENABLE_US_SERVER = "cordovaEnableUsServer";

	private Indoors indoors;
	private Long buildingId;
	private String apiKey;
	private CallbackContext callbackContext;
	private CallbackContext indoorsCallback;

	private LocalizationStarter localizationStarter;

	private int predefinedRouteSnappingDistance = 0;

	private State state = State.STOPPED;
	private ReentrantLock stateLock = new ReentrantLock();
	private Building building;
	private boolean enableBattery = false;
	private String serverUrl = "api";

	private void setState(State state) {
		if (!stateLock.isHeldByCurrentThread()) {
			Log.w(TAG, "State is not locked by current thread.");
		}

		this.state = state;
	}

	private enum State {
		STOPPED, STARTING, RUNNING, STOPPING;
	}

	private class IncorrectStateException extends Exception {
		public IncorrectStateException(State state, String action) {
			super("Cannot run " + action + " in state " + state);
		}
	}

	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
	}

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		this.callbackContext = callbackContext;

		try {
			stateLock.lock();
			if (action.equals(CORDOVA_START_LOCALIZATION)) {
				this.apiKey = args.getString(0);
				this.buildingId = args.getLong(1);
				startLocalization();
			} else if (action.equals(CORDOVA_STOP_LOCALIZATION)) {
				stopLocalization();
			} else if (action.equals(CORDOVA_GET_VERSION)) {
				getVersion();
			} else if (action.equals(CORDOVA_TO_GEO_LOCATION)) {
				JSONObject position = args.getJSONObject(0);
				toGeoLocation(position.getInt("x"), position.getInt("y"), position.getInt("z"));
			} else if (action.equals(CORDOVA_ENABLE_PREDEFINED_ROUTE_SNAPPING)) {
				predefinedRouteSnappingDistance = args.getInt(0);

				callbackContext.success();
			}
			else if (action.equals(CORDOVA_ENABLE_BATTERY_REPORT)) {
				enableBatteryReport();
			}
			else if (action.equals(CORDOVA_ENABLE_US_SERVER)) {
				enableUsServer();
			} else {
				callbackContext.error("Unknown action " + action);
				return false;
			}

			return true;
		} catch (IncorrectStateException e) {
			callbackContext.error(e.getMessage());
			return true;
		} finally {
			stateLock.unlock();
		}
	}

	private void toGeoLocation(int x, int y, int z) throws IncorrectStateException {
		checkState(CORDOVA_TO_GEO_LOCATION, State.RUNNING);

		Coordinate coordinate = new Coordinate(x, y, z);
		Location location = IndoorsCoordinateUtil.toGeoLocation(coordinate, building);
		callbackContext.success(JSONUtils.toJSON(location));

	}

	private void getVersion() throws IncorrectStateException {
		checkState(CORDOVA_GET_VERSION, State.RUNNING);

		String version = indoors.getCurrentVersion();
		try {
			callbackContext.success(new JSONObject().put("version", version));
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private void enableBatteryReport() throws IncorrectStateException {
		checkState(CORDOVA_ENABLE_BATTERY_REPORT, State.STOPPED);
		enableBattery = true;
		callbackContext.success();
	}

	private void enableUsServer() throws IncorrectStateException {
		checkState(CORDOVA_ENABLE_US_SERVER, State.STOPPED);
		serverUrl = "api-us";
		callbackContext.success();
	}

	private void startLocalization() throws IncorrectStateException {
		checkState(CORDOVA_START_LOCALIZATION, State.STOPPED);

		setState(State.STARTING);
		indoorsCallback = callbackContext;

		localizationStarter = new LocalizationStarter(this);
		localizationStarter.start(this.apiKey, this.serverUrl);
	}

	private void stopLocalization() throws IncorrectStateException {
		checkState(CORDOVA_STOP_LOCALIZATION, State.RUNNING);

		setState(State.STOPPING);

		indoors.removeLocationListener(this);
		IndoorsFactory.releaseInstance(this);

		// reset fields
		indoors = null;
		buildingId = null;
		apiKey = null;
		indoorsCallback = null;
		building = null;

		setState(State.STOPPED);

		callbackContext.success();
	}

	/**
	 * This makes sure that we are in a state that is allowed for this action.
	 * E.g. starting the Localization is only allowed in the state STOPPED
	 */
	private void checkState(String action, State... allowedStates) throws IncorrectStateException {
		// make sure that we are holding a lock, otherwise there could be a race condition
		if (!stateLock.isHeldByCurrentThread()) {
			Log.w(TAG, "State not locked");
		}

		for (State allowedState : allowedStates) {
			if (state == allowedState) {
				return;
			}
		}

		throw new IncorrectStateException(state, action);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == LocalizationStarter.REQUEST_CODE_LOCATION) {
			// Check if the user has really enabled Location services.
			localizationStarter.checkLocationIsEnabled();
		}
	}


	@Override
	public void loadingBuilding(LoadingBuildingStatus loadingBuildingStatus) {
		sendIndoorsCallback("loadingBuilding", JSONUtils.toJSON(loadingBuildingStatus));
	}

	private void sendIndoorsCallback(String callbackName) {
		sendIndoorsCallback(callbackName, null);
	}

	private void sendIndoorsCallback(String callbackName, JSONObject result) {
		JSONObject message = new JSONObject();
		try {
			message.put("callbackName", callbackName);
			message.putOpt("result", result);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}

		PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, message);
		pluginResult.setKeepCallback(true);
		indoorsCallback.sendPluginResult(pluginResult);
	}

	@Override
	public void buildingLoaded(Building building) {
		Log.i(TAG, "Building loaded");
		this.building = building;
		sendIndoorsCallback("buildingLoaded", JSONUtils.toJSON(building));
	}

	@Override
	public void leftBuilding(Building building) {
	}

	@Override
	public void buildingReleased(Building building) {

	}

	@Override
	public void positionUpdated(Coordinate coordinate, int accuracy) {
		Log.i(TAG, "Position updated: " + coordinate);

		sendIndoorsCallback("positionUpdated", JSONUtils.toJSON(coordinate, accuracy));
	}

	@Override
	public void orientationUpdated(float orientation) {
		sendIndoorsCallback("orientationUpdated", JSONUtils.toJSON(orientation));
	}

	@Override
	public void changedFloor(int level, String name) {
		sendIndoorsCallback("changedFloor", JSONUtils.toJSON(level, name));
	}

	@Override
	public void enteredZones(List<Zone> list) {
		sendIndoorsCallback("enteredZones", JSONUtils.toJSON(list));
	}

	@Override
	public void buildingLoadingCanceled() {
		sendIndoorsCallback("buildingLoadingCanceled");

	}

	@Override
	public void connected() {
		indoors = IndoorsFactory.getInstance();

		if (predefinedRouteSnappingDistance > 0) {
			indoors.enablePredefinedRouteSnapping();
			indoors.setPredefinedRouteSnappingMaxDistance(predefinedRouteSnappingDistance);
		}

		LocalizationParameters parameters = new LocalizationParameters();

		if (enableBattery) {
			parameters.setBatteryReportsEnabled(true);
		}

		LocatorParams.setKLoadTiles(false);

		indoors.setLocatedCloudBuilding(this.buildingId, parameters, true);

		indoors.registerLocationListener(this);
		try {
			stateLock.lock();
			setState(State.RUNNING);
		} finally {
			stateLock.unlock();
		}
	}

	@Override
	public void onError(IndoorsException e) {
		PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
		pluginResult.setKeepCallback(true);
		indoorsCallback.sendPluginResult(pluginResult);
	}


	/**
	 * The Android system calls us back
	 * after the user has granted permissions (or denied them)
	 */
	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionResult(int requestCode,
	                                      @NonNull String[] permissions,
	                                      @NonNull int[] grantResults) throws JSONException {
		if (requestCode != LocalizationStarter.REQUEST_CODE_PERMISSIONS) {
			// ignore
			return;
		}

		// Since we have requested multiple permissions,
		// we need to check if any were denied
		for (int grant : grantResults) {
			if (grant == PackageManager.PERMISSION_DENIED) {
				Log.e(TAG, "Cannot continue without permissions.");
				this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, PERMISSION_DENIED_ERROR));
				return;
			}
		}

		localizationStarter.checkLocationIsEnabled();
	}
}
