package rs.indoo.cordova;

import android.location.Location;

import com.customlbs.library.callbacks.LoadingBuildingStatus;
import com.customlbs.library.model.Building;
import com.customlbs.library.model.Zone;
import com.customlbs.shared.Coordinate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by michael on 10/31/16.
 */

final class JSONUtils {
	static JSONObject toJSON(Building building) {
		JSONObject result = new JSONObject();
		try {
			result.put("id", building.getId());
			result.put("name", building.getName());
			result.put("description", building.getDescription());
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	static JSONObject toJSON(Coordinate coordinate, int accuracy) {
		JSONObject result = new JSONObject();
		try {
			result.put("x", coordinate.x);
			result.put("y", coordinate.y);
			result.put("z", coordinate.z);
			result.put("accuracy", accuracy);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	static JSONObject toJSON(float orientation) {
		JSONObject result = new JSONObject();
		try {
			result.put("orientation", (int)orientation);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	static JSONObject toJSON(int level, String name) {
		JSONObject result = new JSONObject();
		try {
			result.put("floorLevel", level);
			result.put("name", name);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	static JSONObject toJSON(LoadingBuildingStatus loadingBuildingStatus) {
		JSONObject result = new JSONObject();
		try {
			result.put("progress", loadingBuildingStatus.getProgress());
			result.put("phase", loadingBuildingStatus.getPhase());
			result.put("step", loadingBuildingStatus.getStep());
			result.put("totalSteps", loadingBuildingStatus.getTotalsteps());
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	static JSONObject toJSON(List<Zone> zones) {
		JSONObject result = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (Zone zone : zones) {
				JSONObject zoneObject = new JSONObject();
				zoneObject.put("name", zone.getName());
				zoneObject.put("description", zone.getDescription());
				zoneObject.put("floorLevel", zone.getFloorLevel());
				JSONArray zonePointsArray = new JSONArray();
				for (Coordinate zonePoint : zone.getZonePoints()) {
					zonePointsArray.put(new JSONObject().put("x", zonePoint.x).put("y", zonePoint.y));
				}
				zoneObject.put("zonePoints", zonePointsArray);
			}
			result.put("zones", array);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	static JSONObject toJSON(Location location) {
		JSONObject result = new JSONObject();
		try {
			result.put("lon", location.getLongitude());
			result.put("lat", location.getLatitude());
			if (location.hasAccuracy() && !Float.isNaN(location.getAccuracy())) {
				result.put("accuracy", location.getAccuracy());
			}
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return result;
	}
}
